//version inicial
var express = require('express'), app = express(),
  port = process.env.PORT || 3001;

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested_With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');
var backdb=require('./dausuario');
var backdbctas=require('./dacuentas');
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/listar",function(req,res){
//console.log(req);
backdb.listar(req,res);
})
app.post("/validarusuario",function(req,res){
  var resp=backdb.validarusuario(req,res);
  console.log(resp);
  return(resp);
})
app.post("/grabarusuario",function(req,res){
  var resp=backdb.grabarusuario(req,res);
  console.log(resp);
  return(resp);
})
app.post("/consultausuarios",function(req,res){
  var resp=backdb.listar(req,res);
  console.log(resp);
  return(resp);
})
app.post("/consultacuenta",function(req,res){
  var resp=backdbctas.listar(req,res);
  console.log(resp);
  return(resp);
})
// app.post("/consultamovimiento",function(req,res){
//   var resp=backdb.grabarusuario(req,res);
//   console.log(resp);
//   return(resp);
// })


app.post("/",function(req,res){
  res.send("Hemos recibido su peticion POST");
})


app.put("/",function(req,res){
  res.send("Hemos recibido su peticion PUT cambiada");
})

app.delete("/",function(req,res){
  res.send("Hemos recibido su peticion DELETE");
})

// app.get('/usuarios', function(req,res){
//     var dbusuarioconexion= dbmlab.conectar('usuario');
//     console.log(dbusuarioconexion);
//     var dbusuario=requestjson.createClient(dbusuarioconexion);
//     dbusuario.get('', function(err, resM, body){
//     if(err) {
//       console.log(err);
//     }
//     else{
//       res.send(body);
//     }
//   });
// })

// app.get('/usuariovalidar', function(req,res){
//     var dbusuarioconexion= dbmlab.conectar('usuario');
//     console.log(dbusuarioconexion);
//     var dbusuario=requestjson.createClient(dbusuarioconexion);
//     dbusuario.get('', function(err, resM, body){
//     if(err) {
//       console.log(err);
//     }
//     else{
//       res.send(body);
//     }
//   });
// })
// app.post('/usuarios', function(req,res){
//   clienteMlab.post('',req.body,function(err, resM, body){
//     res.send(body);
//   });
// })
